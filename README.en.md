1. Webman_tool

   #### 介绍

   给webman框架定制开发的工具

   1：参数接受的验证（是否必传，是否符合规则），可以让写的代码优雅又参数安全

   2：简单的token类

   

   参数接受是基于TP8.x的验证器修改的，也就是说内置规则可以看TP的文档：https://doc.thinkphp.cn/v8_0/rule_buildin.html

   #### 软件架构

   仓库开源地址：https://gitee.com/wekyun/webman_tool

   composer主页地址：https://packagist.org/packages/wekyun/tool

   验证规则文档：https://doc.thinkphp.cn/v8_0/rule_buildin.html

   

   #### 安装教程

   1.  在webmen的框架根目录运行 `composer require wekyun/tool`

   #### 部署说明

   1. 需要在config目录下创建 wekyun.php 文件如下。

      ```php
      ├── app                           应用目录
      ├── config                        配置目录
      │   ├── wekyun.php                check配置文件
      ```

      

   2. 在 wekyun.php中粘贴如下代码

      ```
      <?php
      //配置文件名要改成 wekyun.php 放在webman项目的根目录的config的根目录下
      return [
          'def' => [
              'err_code' => 203,//没生效
              //自定义错误的处理方法 $msg：错误提示   也可以去接管抛出的异常：Wekyun\Tool\exception\ValidateException
      //        'err_func' => function ($msg) {
      //        },
              //此配置为必须,配置需要使用的验证场景类，需要按照目录创建文件写法参考TP的验证器文档
              'mapping' => [
                  'def' => \Wekyun\Tool\validate\defValidate::class,//默认的，也可以自己创建一个，这里改成自己的
                  //'com' => \app\validate\ComValidate::class,//自己定义其他规则文件例如，admin shop 等场景使用的规则文件
              ],
          ],
      ];
      ```

   

   #### 使用文档

   ##### **必传规则如下**：默认值中不能用下面这些规则符号，比如url地址中的 .

   `.` 必传

   `>` 自定义必传提示，常用户前端字段友好提示

   `|` 自定义字段错误提示的别名

   `:` 设置默认值，此标识符和必传字段的 `.`不可以同时使用，有冲突

   在字段特别多的时候，验证后写字段的必传时，这个插件非常香。

   **代码的方法演示**

   提示：下面代码中 $req->checkAll('com', ['name', 'age', 'sex']);  com是验证的规则文件

   ```
   <?php
   
   namespace app\controller;
   
   use Wekyun\Tool\Req;
   
   class IndexController
   {
       public function index(Req $req)
       {
           //checkAll：方法是接受验证所有的参数（get和post）,只要传递都接收，不止第二个字段设定的
           //com : 是指验证参数要使用的的规则是哪个，这个需要在wekyun.php这个配置文件的 mapping 中先设置好
           //第二个参数的数组：是指定接受的参数字段
           $req->checkAll('com', ['name', 'age', 'sex']);
   
   
           //checkGet：是接受验证所有get传递的参数
           $req->checkGet();
   
           //checkPost：是接受验证所有post传递的参数
           $req->checkPost();
   
           //checkOnlyAll：是接受指定的参数(get和post)，下面只接收name,别的传了也不管
           $req->checkOnlyAll('com', ['name']);
   
           //checkOnlyGet：是接受指定的参数(get)，下面只接收age,别的传了也不管
           $req->checkOnlyGet('com', ['age']);
   
           //checkOnlyPost：是接受指定的参数(post)，下面只接收sex,别的传了也不管
           $req->checkOnlyPost('com', ['sex']);
   
       }
   
   
   }
   
   ```

   ##### **接收参数的验证**

   提示：$req->checkPost(['email.']);//如果不传，会提示:email不可为空  这里第一个参数没有给 com，会自动用插件的默认的，如果有字段规则，就用，没有就不检测字段的合法性。只会检测是否传递参数

   ```php
   <?php
   
   namespace app\controller;
   
   use Wekyun\Tool\Req;
   
   class IndexController
   {
       public function index(Req $req)
       {
           //接受post参数并验证：   . 代表必传
           $req->checkPost(['email.']);//如果不传，会提示:email不可为空
   
   
           // | 代表给提示的字段，自定义一个提示别名
           $req->checkPost(['email|邮箱.']);//如果不传，会提示:邮箱不可为空
   
   
           // :表示定义一个默认值
           $param = $req->checkPost(['name:wekyun']);
           var_dump($param);//会得到一个数组 ['name'=>'wekyun']
   
           // > 代表自定义提示，提示的信息就是自己定义的
           $req->checkPost(['email.>请输入邮箱']);//如果不传，会提示:请输入邮箱
   
       }
   ```

   

   ##### **自定义规则文件**

   规则文件中会定义字段的合法验证规则

   ```
   <?php
   //插件为了方便，自带了一个默认的规则文件
   namespace Wekyun\Tool\validate;
   
   use Wekyun\Tool\Validate;
   
   class defValidate extends Validate
   {
       protected $rule = [
           'name' => 'max:25',
           'email' => 'email',
           'age' => 'number|between:1,120',
           'mobile' => 'mobile',//验证某个字段的值是否为有效的手机
       ];
   
   
   }
   ```

   

   

   #### 详细啰嗦的说明：

   项目开发中，除了数据库操作以外，最常写的代码便是参数接收和数据校验了。

   很多程序员可能就简单的接受了参数，并不会去验证数据，或者有些程序员只是简单的if去判断一下，但是有时候有些字段必须要验证，字段多了验证数据就写了很多的代码，即便是使用\think\Validate的验证器，也需要去麻烦一下。

   作者跟你们一样懒，但是又想让项目的数据验证规范起来，从最初的验证层，到现在一个插件就搞定，这期间有一个代码进化的过程。

   现在把功能封装成了插件贡献大家使用，也希望各位在数据校验这方面使用起来又简单又方便。

   ### [参数接收](https://www.workerman.net/plugin/126#参数接收)

   ```
   //需要先引入插件
   use Wekyun\WebmanLib\Check;
   
   //基础使用:$param就是接收的参数
   $param = Check::checkAll('com', ['name', 'age', 'sex']);
   var_dump($param);
   ```

   **这里来解释一下两个参数，第一个参数是com,第二个参数是一个数组**

   #### 第一个参数com

   这个参数是配置文件check.php中定义的，对应mapping的com别名。此文件需要在使用之前配置，**如何配置看下面的使用前的配置**，com是告诉插件，要使用哪个TP验证规则文件的规则，你可以定义其他的规则文件，并在配置mapping中注册别名。

   ```
   'mapping' => [
           'com' => \app\common\validate\Common::class,
       ],
   ```

   #### 第二个参数数组

   这第二个参数就是你要接收的参数字段了，如代码这样定义，就可以接受参数，但是如果客户端发送了没有接收的参数，也会被接收，如果想要接收自己指定的参数可以这么写：

   ```
   $param = Check::checkOnlyAll('com', ['name', 'age', 'sex']);
   var_dump($param);
   ```

   checkOnlyAll方法的意思就是验证接收指定的字段，没有指定的字段是不会接收的。

   ### [参数验证与默认值](https://www.workerman.net/plugin/126#参数验证与默认值)

   ##### 字段必传（.）

   必传验证很简单，`name.` 写法就是验证name传递参数是否有值，0，null，false,都算是值，只有空字符串不算传递了参数。

   ```
   Check::checkAll('com', ['name.', 'age', 'sex']);
   ```

   ##### 默认值（:）

   `age:18` 就是给了参数默认值，如果接受的参数这个字段没有值或者没传，就会给指派的默认值。

   **需要注意的是，默认值和必传不要同时写**

   ```
   Check::checkAll('com', ['name.', 'age:18', 'sex']);
   ```

   ##### 错误提示的字段别名（|）

   `name|用户名` 就是给字段name自定义了错误提示时的别名，如果字段name没有传递，或者不符合验证规则，就会提示`用户名`怎么样，如果不设置别名，就会提示`name`怎么样。

   使用时需要注意，|必须跟在字段name面，在必传.的前面，否则无法正常使用。

   ```
   Check::checkAll('com', ['name|用户名.', 'age:18', 'sex']);
   ```

   ##### 自定义完整必传错误提示(>)

   必传字段name没有传递，就会提示 `请输入用户名`，这在用户端提交表单的时候，验证必传字段十分有效。

   需要注意的是，自定义的提示信息优先级不会大于TP的验证错误提示，也就是说，如果在验证规则文件中，设置了字段的其他错误验证规则和验证错误提示，那么验证规则的验证提示是优先级最高的。

   ```
   Check::checkAll('com', ['name.>请输入用户名', 'age:18', 'sex']);
   ```

   ### [使用规则总结](https://www.workerman.net/plugin/126#使用规则总结)

   `.` 必传

   `>` 自定义必传提示，常用户前端字段友好提示

   `|` 自定义字段错误提示的别名

   `:` 设置默认值，此标识符和必传字段的 `.`不可以同时使用，有冲突

   在字段特别多的时候，验证后写字段的必传时，这个插件非常香。
