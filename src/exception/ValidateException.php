<?php
declare (strict_types=1);

namespace Wekyun\Tool\exception;

/**
 * 数据验证异常
 */
class ValidateException extends \Exception
{
}
