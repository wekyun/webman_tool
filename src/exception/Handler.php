<?php

namespace Wekyun\Tool\exception;

use Webman\Exception\ExceptionHandler;
use support\exception\BusinessException;
use Throwable;
use Webman\Http\Request;
use Webman\Http\Response;
use Wekyun\Tool\exception\ValidateException;

class Handler extends ExceptionHandler
{
    public $dontReport = [
        BusinessException::class,
    ];

    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    public function render(Request $request, Throwable $exception): Response
    {
        if (($exception instanceof BusinessException) && ($response = $exception->render($request))) {
            return $response;
        }
        //可以自己定义：Check插件抛出的验证错误
        if ($exception instanceof ValidateException) {
            $err_data = ['status' => false, 'msg' => $exception->getMessage(), 'code' => $exception->getCode()];
            return json($err_data);//改成自己的
        }
        return parent::render($request, $exception);
    }

}