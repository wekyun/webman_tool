<?php
/**
 * 插件提供的全局助手函数
 * */

use Wekyun\Tool\Validate;

/**
 * 生成验证对象
 * @param array|string $validate 验证器类名或者验证规则数组
 * @param array $message 错误提示信息
 * @param bool $batch 是否批量验证
 * @param bool $failException 是否抛出异常
 * @return Validate
 */
if (!function_exists('wek_validate')) {
    function wek_validate($validate = '', array $message = [], bool $batch = false, bool $failException = true): Validate
    {
        if (is_array($validate) || '' === $validate) {
            $v = new Validate();
            if (is_array($validate)) {
                $v->rule($validate);
            }
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                [$validate, $scene] = explode('.', $validate);
            }
//        $class = wek_str_contains($validate, '\\') ? $validate : '';
            $v = new $validate();
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }
        return $v->message($message)->batch($batch)->failException($failException);
    }
}

