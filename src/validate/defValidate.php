<?php

namespace Wekyun\Tool\validate;

use Wekyun\Tool\Validate;

class defValidate extends Validate
{
    protected $rule = [
        'name' => 'max:25',
        'email' => 'email',
        'age' => 'number|between:1,120',
        'mobile' => 'mobile',//验证某个字段的值是否为有效的手机
    ];


}